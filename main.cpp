#include <graphics.h>

char board[3][3] = { {'_','_','_'},{'_','_','_'},{'_','_','_'} };
char pieceType = 'o';

int main() {
	initgraph(600, 600);
	//初始化棋盘
	void initDraw();
	//检测玩家是否胜利
	bool checkWin(char x);
	//检测是否出现平局
	bool checkDraw();
	//绘制棋子
	void DrawPiece();
	//绘制文本提示信息
	void DrawTipText();
	bool running = true;
	ExMessage mesg;

	DWORD begin_time = GetTickCount();
	BeginBatchDraw();
	while (running)
	{
		while(peekmessage(&mesg,EX_MOUSE))
		{
			if (mesg.message == WM_LBUTTONDOWN)
			{
				int x = mesg.x;
				int y = mesg.y;

				int index_x = x / 200;
				int index_y = y / 200;

				//落子
				if (board[index_y][index_x] == '_') {
					board[index_y][index_x] = pieceType;

					//切换类型
					if (pieceType == 'o')
					{
						pieceType = 'x';
					}
					else {
						pieceType = 'o';
					}
				}
			}
		}
		cleardevice();
		
		initDraw();
		DrawPiece();
		DrawTipText();

		FlushBatchDraw();

		//判断
		if (checkWin('o'))
		{
			MessageBox(GetHWnd(), _T("o玩家获胜！"), _T("恭喜！"), MB_OK);
			running = false;
		}
		else if (checkWin('x'))
		{
			MessageBox(GetHWnd(), _T("x玩家获胜！"), _T("恭喜！"), MB_OK);
			running = false;
		}
		else if (!checkDraw()){
			MessageBox(GetHWnd(), _T("平局！"), _T("游戏结束！"), MB_OK);
			running = false;
		}
		//动态延时
		DWORD end_time = GetTickCount();
		DWORD delta_time = end_time - begin_time;
		if (delta_time < 1000 / 60)
		{
			Sleep(1000 / 60 - delta_time);
		}
		
		
	}
	EndBatchDraw();
	return 0;
}

void initDraw() {
	line(0, 200, 600, 200);
	line(0, 400, 600, 400);
	line(200, 0, 200, 600);
	line(400, 0, 400, 600);
}

//检测玩家是否胜利
bool checkWin(char x) {
	if (board[0][0] == x && board[0][1] == x && board[0][2] == x)
		return true;
	if (board[1][0] == x && board[1][1] == x && board[1][2] == x)
		return true;
	if (board[2][0] == x && board[2][1] == x && board[2][2] == x)
		return true;
	if (board[0][0] == x && board[1][0] == x && board[2][0] == x)
		return true;
	if (board[0][1] == x && board[1][1] == x && board[2][1] == x)
		return true;
	if (board[0][2] == x && board[1][2] == x && board[2][2] == x)
		return true;
	if (board[0][0] == x && board[1][1] == x && board[2][2] == x)
		return true;
	if (board[0][2] == x && board[1][1] == x && board[2][0] == x)
		return true;
	return false;
}
//检测是否出现平局
bool checkDraw() {
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++) {
			if (board[i][j] == '-')
			{
				return false;
			}
		}
	}
	return true;
}
//绘制棋子
void DrawPiece() {
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++) {
			switch (board[i][j])
			{
			case 'o':
				circle(200 * j + 100, 200 * i + 100, 100);
				break;
			case 'x':
				line(200 * j, 200 * i, 200 * (j + 1), 200 * (i + 1));
				line(200 * (j + 1), 200 * (i), 200 * j, 200 * (i + 1));
			case '_':
				break;
			default:
				break;
			}
		}
	}
}
//绘制文本提示信息
void DrawTipText() {
	static TCHAR str[64];
	_stprintf_s(str, _T("当前落子为：%c"), pieceType);

	settextcolor(RGB(225,175,45));
	outtextxy(0,0,str);
}
